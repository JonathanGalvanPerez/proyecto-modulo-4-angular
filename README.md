## Proyecto del Módulo 4
En este repositorio se encuentra el proyecto de la semana 4 del curso **Desarrollo de páginas con Angular** de la *Universidad Austral*

### Instrucciones de Instalación
```
npm install
```

### Despliegue
_Dirigete a la subcarpeta **agenda-angular/express-server**_

    node app.js
	
_Dirigete a la carpeta **agenda-angular**_

    ng serve

_Abre el navegador e ingresa a http://localhost:4200/_

### Unit Testing con Karma
_Dirigete a la subcarpeta **agenda-angular/express-server**_

    node app.js

_Dirigete a la carpeta **agenda-angular**_

    ng test

### Testing End-to-End con Protractor
_Dirigete a la carpeta **agenda-angular**_

    ng e2e
    
El codigo de las pruebas se encuentra de la carpeta **angular-agenda/e2e/src** principalmente en el archivo **app.e2e-spec.ts**.
Su sintaxis esta basada en Jasmine, libreria que usamos para las pruebas unitarias